#!/bin/bash

#    C64Movie (c) 2020 Zarko Zivanov

#    C64Movie is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################
#                          Variables
################################################################

VERSION="1.1"

# directories
PNGDIR="PNG"
KLADIR="KOALA"
mkdir -p "$PNGDIR"
mkdir -p "$KLADIR"

# number of threads for Koala image conversion = number of physical cores
THREADS_NUM=$( ls -d /sys/devices/system/cpu/cpu[[:digit:]]* | wc -w )

# movie conversion parameters
# MAX_FRAMES=$(( (16*1024*1024-262144)/10001 )) # MAX 1651 Koala frames
START_TIME=""       # video start time
END_TIME=""         # video end time
DURATION=""         # clip duration
MOVIE_AUDIO=""      # separate audio file
AUDIO_FREQ=4065     # empirical value for audio conversion frequency for PAL
FPS_EXTRACT=6.25    # empirical value video conversion FPS for PAL
# check if there is a locally installed node.js
NODE_JS="$(find ./ -maxdepth 1 -type d -name 'node*' -print -quit)"
if [ -d "$NODE_JS" ]; then
    NODE_JS="$NODE_JS/bin/node"
else
    NODE_JS=$(which node)
fi
RETROPIXELS=$(which retropixels)    # path to retropixels
C64_PLAYER="movie-loop.prg"         # name of C64 player file

# first stage to be executed
START_AT=1
STAGE=0

################################################################
#                        Helper functions
################################################################


# color echo
# - to change text color, use [r],[g],[b],[m],[c],[n]
# - -c NN - center text on NN width
# - -n - no newline
function colecho {
    nonewl=0
    width=0
    if [ "$1" == "-c" ]; then
        width=$2
        shift 2
    fi
    if [ "$1" == "-n" ]; then
        nonewl=1
        shift
    fi
    output="$1"
    chars=${output//\[[a-z]\]/}
    if [ "$2" == "" ]; then output="${output}\e[00m"; fi
    output=${output//\[r\]/\\e\[01;31m}
    output=${output//\[g\]/\\e\[01;32m}
    output=${output//\[b\]/\\e\[01;34m}
    output=${output//\[m\]/\\e\[01;35m}
    output=${output//\[c\]/\\e\[01;36m}
    output=${output//\[n\]/\\e\[00m}
    if [ "$width" != "0" ]; then
        textsize=${#chars}
        if [ $width -gt $textsize ]; then
            span=$((($width - $textsize) / 2))
            printf "%${span}s" ""
        fi
    fi
    if [ $nonewl -eq 0 ]; then
        echo -e "$output"
    else
        echo -n -e "$output"
    fi
}

function usage {
    colecho "\n[g]Script for conversion of movies to C64+REU format with successive Koala frames and"
    colecho "[g]4bit sample audio. Code for C64 playback: https://github.com/vbguyny/c64kernel"
    colecho "\nC64Movie v${VERSION} (c) 2020 Zarko Zivanov, GPL 3, www.onceuponabyte.org"
    colecho "\nUsage: [c]$0[n] [g][OPTIONS] [c]<movie name>"
    colecho "\nOptions:"
    colecho "[g]-b[n] <HH:MM:SS> Start time for frame extraction, default is 00:00:00"
    colecho "[g]-e[n] <HH:MM:SS> End time for frame extraction, default is end of the movie"
    colecho "[g]-p[n] <NUMBER>   FPS for frame extraction, default is $FPS_EXTRACT"
    colecho "[g]-f[n] <NUMBER>   Frequency for audio conversion, default is $AUDIO_FREQ"
    colecho "[g]-t[n] <NUMBER>   Number of threads for Koala image conversion, default is $THREADS_NUM"
    colecho "[g]-s[n] <NUMBER>   Starting stage for movie conversion, default is $START_AT"
    colecho "              1-Frame extraction, 2-Audio extraction, 3-Koala conversion"
    colecho "              4-Frames concatenation, 5-Audio conversion, 6-REU image assembly"
    colecho "              7-C64 player setup, 8-Run in VICE emulator"
    colecho "[g]-h[n]            This help"
    colecho "\n[c]Script requirements:"
    colecho "   packages: [g]ffmpeg, sox, node.js, npm, python3[n] (apt-get install ffmpeg sox libsox-fmt-mp3 gcc npm)"
    colecho "   python: [g]numpy, numba 0.48, librosa[n] (pip install numba==0.48, pip install librosa)"
    colecho "   standard tools: [g]bash, sed, dd, date, xargs, find"
    colecho "   image conversion: [g]retropixels[n] (npm install -g retropixels)"
    colecho ""
}

function error {
    colecho "\n[r]An error occured:"
    cat "$MOVIE_NAME.err"
    rm "$MOVIE_NAME.err" 2>/dev/null
    echo ""
    exit 1
}

################################################################
#                  Command line parsing
################################################################

# extract command line options
while getopts ":hb:e:p:f:t:s:a:" opt; do
  case $opt in
    h)
        # show help
        usage
        exit 0
        ;;
    b)
        # movie begin time
        START_TIME=$OPTARG
        ;;
    e)
        # movie end time
        END_TIME=$OPTARG
        ;;
    f)
        # audio conversion frequency
        AUDIO_FREQ=$OPTARG
        ;;
    p)
        # movie frame extraction fps
        FPS_EXTRACT=$OPTARG
        ;;
    t)
        # number of threads
        THREADS_NUM=$OPTARG
        ;;
    s)
        # starting stage
        START_AT=$OPTARG
        ;;
    a)
        # separate movie audio
        MOVIE_AUDIO=$OPTARG
        if ! [ -f "$MOVIE_AUDIO" ]; then
            colecho "\n[r]Audio file '$MOVIE_AUDIO' not found.\n"
            exit 1
        fi
        ;;
    \?)
        colecho "\n[r]Invalid option: -$OPTARG\n" >&2
        usage
        exit 1
        ;;
    :)
        colecho "\n[r]Option -$OPTARG requires an argument.\n" >&2
        usage
        exit 1
        ;;
  esac
done
shift $((OPTIND-1))

# check if movie file exists
MOVIE="$1"
if [ "$MOVIE" == "" ]; then
    usage
    exit 1
elif ! [ -f "$MOVIE" ]; then
    colecho "\n[r]Video file '$MOVIE' not found.\n"
    exit 1
fi

# extract movie name
MOVIE_NAME=$(basename -- "$MOVIE")
MOVIE_NAME="${MOVIE_NAME%.*}"

# movie duration
MDURATION=$(ffprobe -i "$MOVIE" -sexagesimal -show_entries format=duration -v quiet -of csv="p=0" 2>./"$MOVIE_NAME.err")
if [ $? -gt 0 ]; then error; fi
MDURATION="${MDURATION%.*}"

# convert movie begin and end time to ffmpeg options, check duration
FFMPEGOPT=""
DURATIONS=0
if [ "$START_TIME" != "" ]; then
    FFMPEGOPT="$FFMPEGOPT1 -ss $START_TIME"
    if [ "$END_TIME" != "" ]; then
        # calculate duration between end and begin time
        DURATION=$(( $(date -d "$END_TIME" "+%s") - $(date -d "$START_TIME" "+%s") ))
        DURATIONS=$((DURATION%60))
        DURATIONM=$((DURATION/60))
        DURATION=$(printf "00:%02d:%02d" $DURATIONM $DURATIONS)
        FFMPEGOPT="$FFMPEGOPT -t $DURATION"
    else
        DURATION=$(( $(date -d "$MDURATION" "+%s") - $(date -d "$START_TIME" "+%s") ))
        DURATIONS=$((DURATION%60))
        DURATIONM=$((DURATION/60))
        DURATION=$(printf "00:%02d:%02d" $DURATIONM $DURATIONS)
    fi
elif [ "$END_TIME" != "" ]; then
    # if there is only end time, that is the duration
    FFMPEGOPT="$FFMPEGOPT -t $END_TIME"
    DURATION=$END_TIME
else
    DURATION=$MDURATION
fi
DURATIONS=$(( $(date -d "$DURATION" "+%s") - $(date -d "00:00:00" "+%s") ))
if [ $DURATIONS -gt 252 ] && [ $START_AT -le 2 ]; then
    colecho "\n[r]Selected clip is is too long: $DURATION! Max duration is 04m12s.\n"
    exit 1
fi

# movie width
MWIDTH=$(ffprobe -v error -select_streams v:0 -show_entries stream=width -of csv=s=x:p=0 "$MOVIE" 2>./"$MOVIE_NAME.err")
if [ $? -gt 0 ]; then error; fi
FFMPEGSCALE=""
if [ $MWIDTH -gt 640 ]; then
    FFMPEGSCALE=",scale=640:-1"
fi

colecho "\n[c]Starting conversion of '$MOVIE' from stage $START_AT."

# extract every Nth frame of a movie into PNG directory
STAGE=$((STAGE+1))
if [ $START_AT -le $STAGE ]; then
    rm $PNGDIR/*.png
    colecho "\n[g]Stage $STAGE: Extracting PNG frames from the '$MOVIE'..."
    ffmpeg -loglevel 16 $FFMPEGOPT -i "$MOVIE" -vf "fps=${FPS_EXTRACT}${FFMPEGSCALE}" -vsync vfr $PNGDIR/img_%04d.png 2>./"$MOVIE_NAME.err"
    if [ $? -gt 0 ]; then error; fi
fi

# extract audio from video to ogg file
STAGE=$((STAGE+1))
if [ $START_AT -le $STAGE ]; then
    colecho "\n[g]Stage $STAGE: Extracting audio from the '$MOVIE'..."
    rm "$MOVIE_NAME.ogg" 2>/dev/null
    ffmpeg -loglevel 16 $FFMPEGOPT -i "$MOVIE" -af "highpass=f=200, lowpass=f=8000" -vn -acodec libvorbis "$MOVIE_NAME.ogg" 2>./"$MOVIE_NAME.err"
    if [ $? -gt 0 ]; then error; fi
fi

# convert all PNG files to Koala format, with THREADS_NUM threads
STAGE=$((STAGE+1))
if [ $START_AT -le $STAGE ]; then
    rm $KLADIR/*.kla
    colecho "\n[g]Stage $STAGE: Converting PNG frames to Koala format using $THREADS_NUM thread(s)..."
    colecho "(this step is time consuming, could be a couple of minutes)"
    find $PNGDIR/*.png -printf "%f\0" | xargs -0 -I {} -n1 -P$THREADS_NUM $NODE_JS $RETROPIXELS -p pepto -c rgb PNG/{} $KLADIR/{}.kla 1>/dev/null 2>./"$MOVIE_NAME.err"
    if [ $? -gt 0 ]; then error; fi
fi

# copy all koala images into one file
STAGE=$((STAGE+1))
if [ $START_AT -le $STAGE ]; then
    colecho "\n[g]Stage $STAGE: Concatenating Koala images into '$MOVIE_NAME.koala'..."
    rm "$MOVIE_NAME.koala" 2>/dev/null
    for kla in $KLADIR/*.kla; do
        dd status=none iflag=skip_bytes skip=2 if=$kla >> "$MOVIE_NAME.koala" 2>./"$MOVIE_NAME.err"
        if [ $? -gt 0 ]; then error; fi
    done
fi

STAGE=$((STAGE+1))
if [ $START_AT -le $STAGE ]; then
    colecho "\n[g]Stage $STAGE: Converting audio to 4bit samples..."
    # convert audio to 4bit raw format
    rm "$MOVIE_NAME.4bit" 2>/dev/null
    ./8to4bit.py "$MOVIE_NAME.ogg" "$MOVIE_NAME.4bit" "$AUDIO_FREQ" 2>./"$MOVIE_NAME.err"
    if [ $? -gt 0 ]; then error; fi
fi

# assemble the reu image
STAGE=$((STAGE+1))
if [ $START_AT -le $STAGE ]; then
    colecho "\n[g]Stage $STAGE: Assembling REU image '$MOVIE_NAME.reu'..."
    # create empty 16MB file
    rm "$MOVIE_NAME.reu" 2>/dev/null
    dd status=none bs=1M count=16 if=/dev/zero of="$MOVIE_NAME.reu" 2>./"$MOVIE_NAME.err"
    if [ $? -gt 0 ]; then error; fi
    # insert audio into reu file
    dd status=none conv=notrunc seek=0 if="$MOVIE_NAME.4bit" of="$MOVIE_NAME.reu" 2>./"$MOVIE_NAME.err"
    if [ $? -gt 0 ]; then error; fi
    #insert video into reu file
    dd status=none conv=notrunc oflag=seek_bytes seek=524288 if="$MOVIE_NAME.koala" of="$MOVIE_NAME.reu" 2>./"$MOVIE_NAME.err"
    if [ $? -gt 0 ]; then error; fi
fi

# change c64 player movie data
STAGE=$((STAGE+1))
if [ $START_AT -le $STAGE ]; then
    colecho "\n[g]Stage $STAGE: Setting up player '$MOVIE_NAME.prg'..."
    cp "$C64_PLAYER" "$MOVIE_NAME.prg"
    maxvcount_lo=$((0xBA54))
    maxvcount_hi=$((0xBA58))
    klasize=$(stat --printf="%s" "$MOVIE_NAME.koala")
    num_frames=$((klasize/10001))
    num_frames_lo=$((num_frames & 0xFF))
    num_frames_hi=$((num_frames >> 8))
    printf "$(printf '\\x%02X' $num_frames_lo)" | dd status=none oflag=seek_bytes of="$MOVIE_NAME.prg" seek=$maxvcount_lo count=1 conv=notrunc 2>./"$MOVIE_NAME.err"
    if [ $? -gt 0 ]; then error; fi
    printf "$(printf '\\x%02X' $num_frames_hi)" | dd status=none oflag=seek_bytes of="$MOVIE_NAME.prg" seek=$maxvcount_hi count=1 conv=notrunc 2>./"$MOVIE_NAME.err"
    if [ $? -gt 0 ]; then error; fi
fi

# run the video in VICE
STAGE=$((STAGE+1))
colecho "\n[g]Stage $STAGE: Running Vice emulator with:"
colecho "[c]x64sc -reu -reusize 0x4000 -reuimage '$MOVIE_NAME.reu' '$MOVIE_NAME.prg'\n"
x64sc -silent -reu -reusize 0x4000 -reuimage "$MOVIE_NAME.reu" "$MOVIE_NAME.prg" 2>./"$MOVIE_NAME.err" &
if [ $? -gt 0 ]; then error; fi
rm "$MOVIE_NAME.err" 2>/dev/null

