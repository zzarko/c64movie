#!/usr/bin/env python3

# This conversion is made by Durandal on this CSDB forum topic:
# https://csdb.dk/forums/?roomid=11&topicid=143779
 
import librosa
import numpy
import sys

if len(sys.argv) != 4:
    print("Usage: %s <input file> <output file> <frequency>")
    exit(1)

filename = sys.argv[1]
outname = sys.argv[2]
frequency = int(sys.argv[3])

y, sr = librosa.load(filename, sr=frequency, mono=True) #load file

numpy.clip(y, -1, 1, y) #Clip samples
norm = librosa.mu_compress(y, mu=15, quantize=True) #mu-compress and quantize to 16 different values
norm = norm + 8 #offset the samples values

bin8 = numpy.uint8(norm) #convert to 8-bit unsigned

# convert to two nibbles per byte
bin8_1 = bin8[0::2]
bin8_2 = bin8[1::2]
if len(bin8_2) < len(bin8_1): bin8_2 = numpy.concatenate((bin8_2,numpy.array([0])))
bin4 = numpy.array([ x[0]*16 + x[1] for x in zip(bin8_1, bin8_2) ], dtype=numpy.uint8)

# write output file
out4bit = open(outname,'wb')
bin4.tofile(out4bit)
