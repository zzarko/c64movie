# C64Movie

C64Movie is Bash script that generates audio and video data for movie player from project [VBGuyNY C64 Kernel](https://github.com/vbguyny/c64kernel).
Michael Battaglia was kind enough to make a player code more stable for PAL, and also to add playing loop.

Video is a sequence of Koala frames, while audio is based on 4 bit samples.

Generated movie can be played in VICE with 16MB REU enabled:

`x64sc -reu -reusize 0x4000 -reuimage <movie reufile> <movie prg file>`

Audio conversion is now better thanks to CSDB user Durandal:
[Sample audio on C64, how to do a better conversion?](https://csdb.dk/forums/?roomid=11&topicid=143779)

## Prerequisites

    colecho "   packages: [g]ffmpeg, sox, node.js, npm, python3[n] (apt-get install ffmpeg sox libsox-fmt-mp3 npm)"
    colecho "   python: [g]numpy, numba 0.48, librosa[n] (pip install numba==0.48, pip install librosa)"
    colecho "   image conversion: [g]retropixels[n] (npm install -g retropixels)"


Standard Linux tools: bash, sed, dd, date, xargs, find

Packages: ffmpeg, sox, gcc, node.js, npm

* On Debian-based distros, install with `apt-get install ffmpeg sox libsox-fmt-mp3 gcc npm`

Image conversion: retropixels (npm install -g retropixels)

* if you get errors while running retropixels, you may have too old Node.js version.
In that case, download newest version and unpack it into C64Movie directory (after unpacking, there should be "node-..." directory).

## Usage

`./C64Movie.sh [OPTIONS] <movie name>`

| Option        | Description                                                      |
|---------------|------------------------------------------------------------------|
| -b <HH:MM:SS> | Start time for frame extraction, default is 00:00:00             |
| -e <HH:MM:SS> | End time for frame extraction, default is end of the movie       |
| -p <NUMBER>   | FPS for frame extraction, default is 6.25                        |
| -f <NUMBER>   | Frequency for audio conversion, default is 4065                  |
| -t <NUMBER>   | Number of threads for Koala image conversion                     |
| -s <NUMBER>   | Starting stage for movie conversion, default is 1                |
|               | 1-Frame extraction, 2-Audio extraction, 3-Koala conversion       |
|               | 4-Frames concatenation, 5-Audio conversion, 6-REU image assembly |
|               | 7-C64 player setup, 8-Run in VICE emulator                       |
| -h            | Help                                                             |

Values for video FPS and audio frequency were determined empirically for PAL C64.

## Files

* `8to4bit.py` - Helper program to convert audio to 4-bit samples
* `C64Movie.sh` - Bash script for video conversion
* `movie-loop.prg` - C64 movie player from https://github.com/vbguyny/c64kernel

## Licence

All code in `C64Movie.sh` is released under GNU GPL 3.0 or later licence.

`movie-loop.prg` is Copyright (c) 2019-2020 Michael Battaglia
`8to4bit.py` algorithm by Durandal (CSDB)

## History

* 1.1

    * Conversion to 4-bit samples is now better, thanks to Durandal from CSDB

    * Local Node.js installation is now used automatically

    * GCC isn't a requirement now, but Python 3 is, together with some libraries
    
    * Added error checking through the code
    
    * Changed order of some stages
    
    * Duration parameters are only valid for audio and video extraction (first two stages)
    
    * Movie playing loops at the end, thanks to Michael Battaglia
    
    * Number of frames is now calculated based on size of '.koala' file
    
    * General cleanup of the code

* 1.0 

    * Initial version
